// import 'package:flutter/material.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         // This is the theme of your application.
//         //
//         // Try running your application with "flutter run". You'll see the
//         // application has a blue toolbar. Then, without quitting the app, try
//         // changing the primarySwatch below to Colors.green and then invoke
//         // "hot reload" (press "r" in the console where you ran "flutter run",
//         // or simply save your changes to "hot reload" in a Flutter IDE).
//         // Notice that the counter didn't reset back to zero; the application
//         // is not restarted.
//         primarySwatch: Colors.blue,
//       ),
//       home: IconsCollector(),
//     );
//   }
// }

// // import 'package:flutter_svg_provider/flutter_svg_provider.dart';

// class IconsCollector extends StatefulWidget {
//   const IconsCollector({
//     Key? key,
//   }) : super(key: key);

//   @override
//   _IconsCollectorState createState() => _IconsCollectorState();
// }

// class _IconsCollectorState extends State<IconsCollector> {
//   List<Widget> out = [];

//   List iconlist = [
//     Icon(Icons.ac_unit),
//     Icon(Icons.ac_unit),
//     Icon(Icons.ac_unit),
//     Icon(Icons.ac_unit),
//     Icon(Icons.ac_unit),
//   ];

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     var screensize = MediaQuery.of(context).size;

//     return GridView.builder(
//         scrollDirection: Axis.vertical,
//         itemCount: iconlist.length,
//         gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
//           crossAxisCount: 7,
//         ),
//         itemBuilder: (BuildContext context, int index) {
//           return Draggable(
//               hitTestBehavior: HitTestBehavior.translucent,
//               feedback: Container(
//                   width: screensize.width * 0.1,
//                   height: screensize.height * 0.55,
//                   margin: EdgeInsets.all(12),
//                   decoration: new BoxDecoration(
//                     boxShadow: <BoxShadow>[
//                       BoxShadow(
//                         color: Colors.grey,
//                         offset: Offset(1.1, 1.1),
//                         blurRadius: 10.0,
//                       ),
//                     ],
//                     color: Colors.white,
//                     border: Border.all(width: 5),
//                     // borderRadius: new BorderRadius.all(Radius.circular(30.0)),
//                     shape: BoxShape.circle,
//                   ),
//                   child: ClipRRect(
//                       borderRadius: BorderRadius.circular(30.0),
//                       child: iconlist[index])),
//               data: index,
//               child: Container(
//                   width: screensize.width * 0.1,
//                   height: screensize.height * 0.55,
//                   margin: EdgeInsets.all(12),
//                   decoration: new BoxDecoration(
//                     boxShadow: <BoxShadow>[
//                       BoxShadow(
//                         color: Colors.grey,
//                         offset: Offset(1.1, 1.1),
//                         blurRadius: 10.0,
//                       ),
//                     ],
//                     color: Colors.white,
//                     border: Border.all(width: 5),
//                     // borderRadius: new BorderRadius.all(Radius.circular(30.0)),
//                     shape: BoxShape.circle,
//                   ),
//                   child: ClipRRect(
//                       borderRadius: BorderRadius.circular(30.0),
//                       child: iconlist[index])));
//         });
//   }
// }

