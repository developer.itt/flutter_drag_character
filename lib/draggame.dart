// ignore_for_file: avoid_print, duplicate_ignore, deprecated_member_use, non_constant_identifier_names

import 'dart:async';
import 'package:flutter/material.dart';

GlobalKey<_TestScaffoldState> _myKey = GlobalKey();

class DraggingList extends StatefulWidget {
  const DraggingList(
      {Key? key,
      required this.name,
      required this.isshow,
      required this.index,
      required this.type})
      : super(key: key);

  final String name;
  final bool isshow;
  final int index;
  final String type;

  @override
  State<DraggingList> createState() => _DraggingListState();
}

class _DraggingListState extends State<DraggingList> {
  @override
  Widget build(BuildContext context) {
    print('Array $selected_answer');
    print('SELECTED BOX NAME' +
        ' ' +
        widget.name +
        ' ' +
        widget.type +
        ' ' +
        widget.index.toString());
    singlechar = widget.name;
    istype = widget.type;
    temp = widget.index;
    singlechar = '';

    if (widget.type == 'mainword') {
      if (widget.name == '') {
        return const Text('');
      } else {
        setState(() {
          temp_dropindex.remove(widget.index);
        });
        return FractionalTranslation(
          translation: const Offset(-0.5, -0.5),
          child: SizedBox(
            // color: Colors.red,
            height: 50,
            width: 50,
            child: Opacity(
              opacity: 0.85,
              child: Visibility(
                visible: true,
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Text(
                    widget.name,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ),
          ),
        );
      }
    } else {
      if (selected_answer[widget.index] == '') {
        return const Text('');
      } else {
        setState(() {
          temp_dropindex.remove(widget.index);
        });
        return FractionalTranslation(
          translation: const Offset(-0.5, -0.5),
          child: SizedBox(
            // color: Colors.red,
            height: 50,
            width: 50,
            child: Opacity(
              opacity: 0.85,
              child: Visibility(
                visible: true,
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Text(
                    widget.name,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                  ),
                  alignment: Alignment.center,
                ),
              ),
            ),
          ),
        );
      }
    }
  }
}

class TestScaffold extends StatefulWidget {
  const TestScaffold({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TestScaffoldState();
}

List<String> ans = ["A", "O", "C", "K", "O", "A", "T", "W", "O", "T"];
List<int> matchword = [];
List<int> finalmatchword = [];

String singlechar = '';
String istype = '';
bool showChar = false;
int temp = 0;
int drop_index = 0;
int left_index = 0;
int right_index = 0;
int top_index = 0;
int bottom_index = 0;
String str_left = '';
String str_right = '';
String str_top = '';
String str_bottom = '';
List<String> spelling = ['CAT', 'TO', 'BCA', 'AT', 'TWO', 'COW'];
List<String> ans_word = [];
List<bool> ansSpelling = [];
List<String> arr_latters = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z'
];
List<int> arr_latters_point = [
  1,
  3,
  3,
  2,
  1,
  4,
  2,
  4,
  1,
  8,
  5,
  1,
  3,
  1,
  1,
  3,
  10,
  1,
  1,
  1,
  1,
  4,
  4,
  8,
  4,
  10
];

List<String> arr = [];
int index = 0;
List<String> result = [];
List<int> temp_dropindex = [];
bool dragdone = false;
List<bool> ansshow = [
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  true,
  true
];

// ignore: non_constant_identifier_names
List<String> selected_answer = [];
// ignore: non_constant_identifier_names
List<int> selected_answer_index = [];
// ignore: non_constant_identifier_names

class _TestScaffoldState extends State<TestScaffold> {
  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 100; i++) {
      // boxes.add(Box(boxIndex: i));
      selected_answer_index.add(i);
      selected_answer.add('');
      matchword.add(101);
      ansSpelling.add(false);
    }
  }

  void reloadfrombutton() {
    ans.removeAt(temp);
    _TestScaffoldState();
  }

  void reloadfrombox() {
    _TestScaffoldState();
  }

  void submitClick() {
    print('CLICK' + matchword.toString());

    setState(() {
      finalmatchword.clear();
      finalmatchword.addAll(matchword);
    });
    print('FINALCLICK' + finalmatchword.toString());
  }

  void getSpelling() {
    str_left = '';
    str_right = '';
    str_top = '';
    str_bottom = '';
    arr.clear();

    left_index = (drop_index);
    right_index = (drop_index);
    top_index = (drop_index);
    bottom_index = (drop_index);

    leftSpelling(); //done
    rightSpelling(); //done
    topSpelling(); //done
    bottomSpelling(); //done

    Timer(const Duration(milliseconds: 10), () {
      bool temp = false;
      print(drop_index);
      if (drop_index == 0 ||
          drop_index == 10 ||
          drop_index == 20 ||
          drop_index == 30 ||
          drop_index == 40 ||
          drop_index == 50 ||
          drop_index == 60 ||
          drop_index == 70 ||
          drop_index == 80 ||
          drop_index == 90) {
        // arr.add('left' + ' ' + str_left);
        if (str_left.length > 1) {
          arr.add(str_left);
        }
      } else {
        if (str_right.isEmpty) {
          // arr.add('left' + ' ' + str_left);
          if (str_left.length > 1) {
            arr.add(str_left);
          }
        } else {
          // arr.add('right' + ' ' + str_right);
          if (str_right.length > 1) {
            arr.add(str_right);
          }
        }
      }
      if (str_bottom.length < str_top.length) {
        // arr.add('top' + ' ' + str_top);
        if (str_top.length > 1) {
          arr.add(str_top);
        }
      } else {
        // arr.add('bottom' + ' ' + str_bottom);
        if (str_bottom.length > 1) {
          arr.add(str_bottom);
        }
      }
      print('array ' + arr.toString());

      if (selected_answer[drop_index] != '') {
        if (!temp_dropindex.contains(drop_index)) {
          temp_dropindex.add(drop_index);
        }
      }



      
      if (temp_dropindex.length > 1) {
        print('drop_index    ' + temp_dropindex.toString());
        print('second last index   ' +
            temp_dropindex[temp_dropindex.length - 2].toString());
        print('last index   ' +
            temp_dropindex[temp_dropindex.length - 1].toString());             
        int different = temp_dropindex[temp_dropindex.length - 2] - temp_dropindex[temp_dropindex.length - 1];
        print(different);
      }

      // if (ans_word.isEmpty) {
      //   print(selected_answer.length);
      //   for (int j = 0; j < spelling.length; j++) {
      //     if (arr.length == 1) {
      //       if (arr[0] == spelling[j]) {
      //         print('matched index 0 ' + arr[0]);
      //         matchHighlight(arr[0]);
      //       } else {
      //         // unmatchHighlight(arr[0]);
      //       }
      //     }
      //     if (arr.length == 2) {
      //       if (arr[1] == spelling[j]) {
      //         print('matched index 1 ' + arr[1]);
      //         matchHighlight(arr[1]);
      //       } else {
      //         // unmatchHighlight(arr[1]);
      //       }
      //     }
      //   }
      // } else {
      //   if (!temp_dropindex.contains(drop_index)) {
      //     if (matchword.contains(drop_index - 1) ||
      //         matchword.contains(drop_index - 10) ||
      //         matchword.contains(drop_index + 1) ||
      //         matchword.contains(drop_index + 10) ||
      //         matchword.contains(drop_index) ||
      //         temp_dropindex.contains(drop_index - 1) ||
      //         temp_dropindex.contains(drop_index - 10) ||
      //         temp_dropindex.contains(drop_index + 1) ||
      //         temp_dropindex.contains(drop_index + 10)) {
      //       for (int j = 0; j < spelling.length; j++) {
      //         print(spelling[j]);

      //         if (arr.length == 1) {
      //           if (arr[0] == spelling[j]) {
      //             print('matched index 0 ' + arr[0]);
      //             matchHighlight(arr[0]);
      //           } else {
      //             // unmatchHighlight(arr[0]);
      //           }
      //         }
      //         if (arr.length == 2) {
      //           temp = true;
      //           // if (arr[1] == spelling[j]) {
      //           //   print('matched index 1 ' + arr[1]);
      //           // matchHighlight(arr[1]);
      //           // }
      //         }
      //       }
      //     } else {
      //       final scaffold = ScaffoldMessenger.of(context);
      //       scaffold.showSnackBar(
      //         const SnackBar(
      //           content: Text('New box must touch existing boxes.'),
      //         ),
      //       );
      //     }
      //   }
      // }

      int count = 0;
      if (temp == true) {
        for (int i = 0; i < spelling.length; i++) {
          if (arr[0] == spelling[i]) {
            count = count + 1;
          }
          if (arr[1] == spelling[i]) {
            count = count + 1;
          }
        }
      }

      if (count == 2) {
        matchHighlight(arr[0]);
        matchHighlight(arr[1]);
      } else {
        // unmatchHighlight(arr[0]);
        // unmatchHighlight(arr[1]);
      }
    });
  }

  // LEFT SPELLING
  void leftSpelling() {
    if (selected_answer[left_index] != '') {
      int temp = left_index;
      var str = left_index.toString();
      var intLast = int.parse(str.substring(str.length - 1));
      for (int j = left_index - 1; j >= left_index - (intLast); j--) {
        // print('INDEX' + j.toString());
        temp = j + 1;
        if (selected_answer[j] == '') {
          break;
        }
      }
      String a = temp.toString();
      var test = int.parse(a.substring(a.length - 1));
      int b = 0;
      for (int k = temp - b; k < temp + (10 - test); k++) {
        str_left = str_left + selected_answer[k];
        // print('LEFT STRING ' + str_left);
        if (selected_answer[k] == '') {
          break;
        }
      }
    }
  }

  // RIGHT SPELLING
  void rightSpelling() {
    if (selected_answer[right_index] != '') {
      int temp = 0;
      var str = right_index.toString();
      var intLast = int.parse(str.substring(str.length - 1));
      for (int j = right_index - 1; j > right_index - (intLast + 1); j--) {
        // print('INDEX' + j.toString());
        temp = j;
        if (selected_answer[j] == '') {
          break;
        }
      }
      for (int k = temp; k < (temp + (11 - intLast)); k++) {
        str_right = str_right + selected_answer[k];
        // print('RIGHT STRING ' + str_right);
        if (selected_answer[k] == '') {
          break;
        }
      }
    }
  }

  // TOP SPELLING
  void topSpelling() {
    if (selected_answer[top_index] != '') {
      int temp = 0;
      var str = top_index.toString();
      var intLast = int.parse(str.substring(str.length - 1));
      print(top_index);
      for (int j = top_index; j >= 0; j -= 10) {
        // print('INDEX' + j.toString());
        temp = j;
        if (selected_answer[j] == '') {
          break;
        }
      }
      for (int k = temp; k < 100; k += 10) {
        str_top = str_top + selected_answer[k];
        // print('TOP STRING ' + str_top);
        if (selected_answer[k] == '') {
          break;
        }
      }
    }
  }

  //BOTTOM SPELLING
  void bottomSpelling() {
    if (selected_answer[bottom_index] != '') {
      int temp = 0;
      var str = top_index.toString();
      var intLast = int.parse(str.substring(str.length - 1));
      for (int j = bottom_index; j >= 0; j -= 10) {
        // print('INDEX' + j.toString());
        temp = j;
        if (selected_answer[j] == '') {
          break;
        }
      }
      for (int k = temp + 10; k < 100; k += 10) {
        str_bottom = str_bottom + selected_answer[k];
        print(str_bottom);
        // print('BOTTOM STRING ' + str_bottom);
        if (selected_answer[k] == '') {
          break;
        }
      }
    }
  }

  //Highlight Function
  void matchHighlight(String str) {
    List<String> arr = str.split('');
    if (!ans_word.contains(str)) {
      ans_word.add(str);
    }
    print('matched word' + matchword.toString());
    print('ans_word' + ans_word.toString());

    for (int i = 0; i < selected_answer.length; i++) {
      for (int j = 0; j < arr.length; j++) {
        if (selected_answer[i] == arr[j]) {
          setState(() {
            matchword.removeAt(i);
            matchword.insert(i, i);
          });
        } else {
          print('NO MATCHED ' + arr.toString());
        }
      }
    }

    // for (int i = 0; i < arr.length; i++) {
    //   List<String> arrCh = arr[i].split('');
    //   for (int j = 0; j < selected_answer.length; j++) {
    //     for (int k = 0; k < arrCh.length; k++) {
    //       setState(() {
    //         matchword.removeAt(j);
    //         matchword.insert(j, 101);
    //       });
    //     }
    //   }
    // }

    // calculateScore(str);
  }

  void unmatchHighlight(String str) {
    print('unmatchHighlight     ' + str);
    // List<String> arrCh = str.split('');
    // for (int j = 0; j < selected_answer.length; j++) {
    //   for (int k = 0; k < arrCh.length; k++) {
    //     setState(() {
    //       matchword.removeAt(j);
    //       matchword.insert(j, 101);
    //     });
    //   }
    // }
  }

  //Calculate Score
  void calculateScore(String str) {
    print('calculateScore');
    List<String> arr = str.split('');
    int score = 0;
    for (int i = 0; i < arr.length; i++) {
      index = arr_latters.indexWhere((element) => element == arr[i]);
      score = score + arr_latters_point[index];
    }
    print('score   ' + score.toString());
  }

  void removehightlight() {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _myKey,
        appBar: AppBar(
          title: const Text('Drag&Drop Demo'),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Center(
                    child: Container(
                  // color: Colors.yellow,
                  alignment: Alignment.center,
                  // child: GridView.count(
                  //   padding: const EdgeInsets.fromLTRB(25, 25, 25, 25),
                  //   crossAxisSpacing: 2,
                  //   mainAxisSpacing: 2,
                  //   crossAxisCount: 10,
                  //   physics: const ClampingScrollPhysics(),
                  //   shrinkWrap: true,
                  //   children: boxes,
                  // ),
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: GridView.builder(
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisSpacing: 2,
                          mainAxisSpacing: 2,
                          crossAxisCount: 10,
                        ),
                        itemCount: selected_answer_index.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              !finalmatchword.contains(index)
                                  ? Draggable<String>(
                                      data: selected_answer[index],
                                      dragAnchorStrategy:
                                          pointerDragAnchorStrategy,
                                      childWhenDragging: Container(
                                        width: 25,
                                        height: 25,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Colors.green,
                                              width: 1.0,
                                            ),
                                            borderRadius:
                                                const BorderRadius.all(
                                                    Radius.circular(5.0))),
                                      ),
                                      onDragStarted: () {
                                        setState(() {
                                          selected_answer[index] != ''
                                              ? dragdone = true
                                              : dragdone = false;
                                          drop_index = index;
                                        });
                                        print('on drag started...' +
                                            'DONEDONE' +
                                            index.toString());
                                        getSpelling();
                                      },
                                      feedback: selected_answer[index] != ''
                                          ? DraggingList(
                                              name: selected_answer[index],
                                              isshow: true,
                                              index: index,
                                              type: 'box',
                                            )
                                          : const Text(''),
                                      child: DragTarget<String>(
                                        builder: (
                                          BuildContext context,
                                          List<dynamic> candidateItems,
                                          List<dynamic> rejectedItem,
                                        ) {
                                          return Container(
                                            width: 25,
                                            height: 25,
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                  color:
                                                      matchword.contains(index)
                                                          ? Colors.blue
                                                          : Colors.lightGreen,
                                                  width: 1.0,
                                                ),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                        Radius.circular(5.0))),
                                            child: Center(
                                              child:
                                                  Text(selected_answer[index]),
                                            ),
                                          );
                                        },
                                        onAccept: (String item) {
                                          setState(() {
                                            dragdone = true;
                                            singlechar = item;
                                            showChar = false;
                                            print(istype);
                                            if (istype == 'box') {
                                              print('drag box');
                                              if (selected_answer[temp] != '') {
                                                if (selected_answer[index] ==
                                                    '') {
                                                  drop_index = index;
                                                  selected_answer
                                                      .removeAt(temp);
                                                  selected_answer.insert(
                                                      temp, '');
                                                  selected_answer
                                                      .removeAt(index);
                                                  selected_answer.insert(
                                                      index, item);
                                                  matchword.removeAt(temp);
                                                  matchword.insert(temp, 101);
                                                  _TestScaffoldState _myapp =
                                                      _TestScaffoldState();
                                                  _myapp.reloadfrombox();
                                                  (context as Element)
                                                      .reassemble();
                                                }
                                              }
                                            } else {
                                              if (selected_answer[index] ==
                                                  '') {
                                                print('drag button');
                                                drop_index = index;
                                                selected_answer.removeAt(index);
                                                selected_answer.insert(
                                                    index, item);
                                                _TestScaffoldState _myapp =
                                                    _TestScaffoldState();
                                                _myapp.reloadfrombutton();
                                                (context as Element)
                                                    .reassemble();
                                              }
                                            }
                                            Timer(
                                                const Duration(
                                                    milliseconds: 10), () {
                                              getSpelling();
                                            });
                                          });
                                        },
                                      ),
                                    )
                                  : Container(
                                      width: 25,
                                      height: 25,
                                      decoration: const BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                      // decoration: BoxDecoration(
                                      //     border: Border.all(
                                      //       color: matchword.contains(index)
                                      //           ? Colors.red
                                      //           : Colors.lightGreen,
                                      //       width: 1.0,
                                      //     ),
                                      //     borderRadius: const BorderRadius.all(
                                      //         Radius.circular(5.0))),
                                      child: Center(
                                        child: Text(selected_answer[index]),
                                      ),
                                    )
                            ],
                          );
                        }),
                  ),
                )),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Stack(
                children: [
                  Container(
                    alignment: Alignment.center,
                    child: GridView.builder(
                        physics: const ClampingScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          crossAxisCount: 5,
                        ),
                        itemCount: ans.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Draggable<String>(
                              data: ans[index],
                              dragAnchorStrategy: pointerDragAnchorStrategy,
                              childWhenDragging: Container(),
                              onDragStarted: () {
                                setState(() {
                                  showChar = true;
                                });
                                print('on drag started...$showChar');
                              },
                              onDragEnd: (val) {
                                setState(() {
                                  showChar = false;
                                });
                                print('on drag END...$showChar');
                              },
                              onDragCompleted: () {
                                print('on drag completed...$showChar');
                              },
                              feedback: ansshow[index] == true
                                  ? DraggingList(
                                      name: ans[index],
                                      isshow: ansshow[index],
                                      index: index,
                                      type: 'mainword',
                                    )
                                  : Container(),
                              child: Container(
                                decoration: const BoxDecoration(
                                    color: Colors.green,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                child: Text(
                                  ans[index],
                                  style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w600),
                                ),
                                alignment: Alignment.center,
                              ),
                            ),
                          );
                        }),
                  ),
                  SizedBox(
                    height: 120,
                    width: MediaQuery.of(context).size.width,
                    child: DragTarget<String>(
                      builder: (
                        BuildContext context,
                        List<dynamic> candidateItems,
                        List<dynamic> rejectedItem,
                      ) {
                        return Container();
                      },
                      onAccept: (String item) {
                        print('on Accept' +
                            item +
                            temp.toString() +
                            ans.length.toString());
                        setState(() {
                          temp_dropindex.remove(temp);
                          selected_answer.removeAt(temp);
                          selected_answer.insert(temp, '');
                          if (dragdone == true) {
                            dragdone = false;
                            ans.add(item);
                          }
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
            Column(
              children: [
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width - 50,
                      decoration: const BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: RaisedButton(
                        color: Colors.green, // background
                        textColor: Colors.white, // foreground
                        onPressed: submitClick,
                        child: const Text('Submit'),
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ));
  }
}
